///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file testharness.cpp
/// @version 1.0
///
/// Perform unit tests
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   20_APR_2021
///////////////////////////////////////////////////////////////////////////////
//#include <stdlib>
#include <iostream>
#include "cat.hpp"

using namespace std;


int main() {
   cout << "Cat unit tests" << endl;
   Cat::initNames();

   srand(100);
   CatEmpire empire;

   cout << "Empire created." << endl;
   cout << "Is empty: " << boolalpha << empire.empty() << endl;
   empire.catList();

   cout << "--------------------------------" << endl;
   
   for(int i = 0; i < 10; i++) {
      Cat* newCat = Cat::makeCat();
      empire.addCat(newCat);
      //cout << newCat->name << endl;
   }

   cout << "--------------------------------" << endl;

   empire.catList();
   cout << "Is empty: " << boolalpha << empire.empty() << endl;

   empire.catGenerations();

}
